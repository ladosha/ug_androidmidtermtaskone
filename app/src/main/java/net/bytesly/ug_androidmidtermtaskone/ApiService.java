package net.bytesly.ug_androidmidtermtaskone;

import net.bytesly.ug_androidmidtermtaskone.model.PlaceholderPost;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("posts")
    Call<List<PlaceholderPost>> getPosts();
}
